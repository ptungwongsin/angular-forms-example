import { AfterViewInit, Component, Optional, SkipSelf } from '@angular/core';
import { FormArray, FormArrayName, FormControl, FormGroupDirective, NgForm } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements AfterViewInit {
  formArray = new FormArray([
    new FormControl('')
  ]);

  constructor(@SkipSelf() @Optional() private ngForm: NgForm,
              @SkipSelf() @Optional() private formGroupDirective: FormGroupDirective,
              @Optional() private formArrayName: FormArrayName) {
    // Template driven forms
    if (this.ngForm) {
      this.ngForm.form.addControl('contacts', this.formArray);
    }
  }

  ngAfterViewInit(): void {
    // Reactive Forms
    // FormArrayName will be available on AfterViewInit
    if (this.formArrayName) {
      this.formArray.controls.forEach(control => this.formArrayName.control.push(control));
      this.formArray = this.formArrayName.control;
    }

    // alternative solution
    // remove formArray in AppComponent and inject it dynamically here
    // if (this.formGroupDirective) {
    //   this.formGroupDirective.form.addControl('contacts', this.formArray);
    // }
  }

  addContact(): void {
    const phoneControl = new FormControl('');
    this.formArray.push(phoneControl);
  }
}
