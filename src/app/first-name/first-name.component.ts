import { AfterViewInit, Component, Optional, SkipSelf, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, NgForm, NgModel, } from '@angular/forms';

@Component({
  selector: 'app-first-name',
  templateUrl: './first-name.component.html',
  styleUrls: ['./first-name.component.scss']
})
export class FirstNameComponent implements AfterViewInit {

  @ViewChild('firstNameField', { static: false }) firstNameControl: NgModel;
  firstName = '';

  private formGroup: FormGroup;

  constructor(@SkipSelf() @Optional() private formGroupDirective: FormGroupDirective,
              @SkipSelf() @Optional() private ngForm: NgForm) {
    // Template driven form
    if (this.ngForm) {
      this.formGroup = this.ngForm.form;
    }
  }

  ngAfterViewInit(): void {
    // Reactive form
    if (this.formGroupDirective) {
      this.formGroup = this.formGroupDirective.control;
    }
    this.formGroup.addControl('firstName', this.firstNameControl.control);
  }
}
