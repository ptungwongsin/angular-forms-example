import { AfterViewInit, ChangeDetectorRef, Component } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  comment = 'This is cool!';
  reactiveForms = new FormGroup({
    contacts: new FormArray([])
  });

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    // wait for child form to complete initialization and detect change to avoid ExpressionChangedAfterItHasBeenCheckedError
    this.changeDetectorRef.detectChanges();
  }

  prettyPrint(obj: any): string {
    return JSON.stringify(obj, null, 2);
  }

  submitForm(formGroup: FormGroup): void {
    formGroup.markAllAsTouched();

    if (formGroup.valid) {
      // do something
    }
  }
}
