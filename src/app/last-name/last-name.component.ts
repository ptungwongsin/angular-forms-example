import { Component, OnInit, Optional, SkipSelf } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-last-name',
  templateUrl: './last-name.component.html',
  styleUrls: ['./last-name.component.scss']
})
export class LastNameComponent implements OnInit {

  formControl = new FormControl('', Validators.required);

  private formGroup: FormGroup;

  constructor(@SkipSelf() @Optional() private ngForm: NgForm,
              @SkipSelf() @Optional() private formGroupDirective: FormGroupDirective) {
    // Template driven form
    if (this.ngForm) {
      this.formGroup = this.ngForm.form;
    }
  }

  ngOnInit() {
    // Reactive forms
    if (this.formGroupDirective) {
      this.formGroup = this.formGroupDirective.form;
    }
    this.formGroup.addControl('lastName', this.formControl);
  }

}
