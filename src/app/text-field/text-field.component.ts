import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Component, EventEmitter, Input, OnInit, Optional, Output, Self } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { CustomErrorStateMatcher } from './custom-error-state-matcher';

@Component({
  selector: 'app-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.scss']
})
export class TextFieldComponent implements ControlValueAccessor, OnInit {

  @Input() placeholder: string;
  @Input() value: string;
  @Output() valueChange = new EventEmitter<string>();
  errorStateMatcher: ErrorStateMatcher;
  isDisabled: boolean;
  onTouched = () => {};

  private _isTextArea: boolean;
  private _onChange = (val: string) => {};

  constructor(@Self() @Optional() private ngControl: NgControl) {
    // provide value accessor for Angular Forms API
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  @Input()
  get isTextArea(): boolean {
    return this._isTextArea;
  }

  set isTextArea(value: boolean) {
    this._isTextArea = coerceBooleanProperty(value);
  }

  ngOnInit(): void {
    if (this.ngControl) {
      this.errorStateMatcher = new CustomErrorStateMatcher(this.ngControl.control as FormControl);
    }
  }

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  writeValue(value: string): void {
    this.value = value;
  }

  onChange(value: string): void {
    this.valueChange.emit(value);
    this._onChange(value);
  }
}
