import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

// This class is for synchronizing error state of provided FormControl and local MatFormField FormControl
export class CustomErrorStateMatcher implements ErrorStateMatcher {
  constructor(private providedFormControl: FormControl) {}

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return  this.providedFormControl.invalid && (this.providedFormControl.touched || this.providedFormControl.dirty);
  }
}
